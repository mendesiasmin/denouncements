class AddTokenToDenouncements < ActiveRecord::Migration
  def up
    add_column :denouncements_plugin_denouncements, :token, :string
  end

  def down
    remove_column :denouncements_plugin_denouncements, :token, :string
  end
end
