class AddSocioEconomicDataToPeople < ActiveRecord::Migration
  def change
    add_column :profiles, :ethnicity,          :string
    add_column :profiles, :disability,         :string
    add_column :profiles, :gender_identity,    :string
    add_column :profiles, :gender_condition,   :string
    add_column :profiles, :sexual_orientation, :string
  end
end
