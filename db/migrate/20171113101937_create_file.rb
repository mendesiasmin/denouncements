class CreateFile < ActiveRecord::Migration
  def up
    create_table :denouncements_plugin_files do |t|

      t.column :denouncement_id, :integer

    # attachment_fu data for all uploaded files
      t.column :size,         :integer  # file size in bytes
      t.column :content_type, :string   # mime type, ex: application/mp3
      t.column :filename,     :string   # sanitized filename
    end
  end

  def down
    drop_table :denouncements_plugin_files
  end


end
