class CreatesDenouncers < ActiveRecord::Migration
  def change
    create_table :denouncements_plugin_denouncers do |t|
      t.timestamps
      t.string  :name,         null: false
      t.string  :email,        null: false
      t.string  :device_token
      t.integer :profile_id
    end
  end
end
