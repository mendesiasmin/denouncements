class AddPersonToDenouncement < ActiveRecord::Migration
  def change
    add_reference :denouncements_plugin_denouncements, :person, index: true
  end
end
