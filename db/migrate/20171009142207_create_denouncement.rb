class CreateDenouncement < ActiveRecord::Migration
  def up
    create_table :denouncements_plugin_denouncements do |t|
      t.datetime :date
      t.string :protocol
      t.string :status
    end
  end

  def down
    drop_table :denouncements_plugin_denouncements
  end
end
