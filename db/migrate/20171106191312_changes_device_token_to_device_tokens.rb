class ChangesDeviceTokenToDeviceTokens < ActiveRecord::Migration
  def up
    rename_column :denouncements_plugin_denouncers, :device_token,
                  :device_tokens
    change_column :denouncements_plugin_denouncers, :device_tokens,
                  "varchar[] USING (string_to_array(device_tokens, ''))",
                  default: []
  end

  def down
    change_column :denouncements_plugin_denouncers, :device_tokens,
                  "varchar USING (array_to_string(device_tokens, ''))"
    rename_column :denouncements_plugin_denouncers, :device_tokens,
                  :device_token
  end
end
