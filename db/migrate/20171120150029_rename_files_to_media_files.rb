class RenameFilesToMediaFiles < ActiveRecord::Migration

  def self.up
    rename_table :denouncements_plugin_files, :denouncements_plugin_media_files
  end

  def self.down
    rename_table :denouncements_plugin_media_files, :denouncements_plugin_files
  end
end
