function setPushToken(token, uuid) {
  $.post('/plugin/denouncements/push_token/set', {
    token: token,
    uuid: uuid
  });
}
