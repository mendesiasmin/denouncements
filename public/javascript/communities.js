$(document).ready(function() {
  var content          = $("#content");
  var communitiesBlock = $("#theme-header .participa-communities-block");
  var profileBlock     = $("#theme-footer .profile-description-block");
  content.prepend(communitiesBlock);
  content.append(profileBlock);

  $('body.controller-search.action-search-communities .main-content h1').html("Todas comunidades");
})
