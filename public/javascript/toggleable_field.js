$(document).ready(function() {
  $('.toggleable-field').on('click', 'input.toggle', function() {
    var wrapper = $(this).closest('div.toggleable-field')
    var field = wrapper.find('input.text-field')

    field.prop('disabled', $(this).is(':checked'))
    field.val('')
  })

  $('.toggleable-select').on('click', 'input.toggle', function() {
    var wrapper = $(this).closest('div.toggleable-select')
    var field = wrapper.find('select.select-field')

    field.prop('disabled', $(this).is(':checked'))
    field.val('')
  })

})

function returnToogleableFieldResponse(el, text) {
  var field = $(el)
  if (text == field.data('alt') || !text) {
    field.parent().find('input[type=\'checkbox\'].toggle').prop('checked', true)
    field.parent().find('.toggle-wrapper').addClass('checked')
    field.attr('disabled', true)
    return ""
  } else {
    return text
  }
}
