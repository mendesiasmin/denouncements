$(document).ready(function() {

  var alreadyClicked = false;
  $('#victims-step').on('click', '.continue-btn', function() {
      var victims = getVictims()
      if(victims.length > 0) {
          if(!alreadyClicked)
              addOffenderForm();
          StepsBar.nextStep()
          Confirmation.setVictims(victims)
      } else {
          showErrorMessage('#victims-step .error-msg',
           '#victims-step .errors-list #victims-empty')
      }
      alreadyClicked = true
      return false
  })

  $('#victims-step').on('click', '.button#add-victim', function() {
    if (checkLastVictim()) {
      addVictimForm()
    } else {
      showErrorMessage('#victims-step .error-msg',
                       '#victims-step .errors-list #victim-no-information')
    }
    return false
  })

  $('#victims-list').on('click', '.victim .remove-victim', function() {
    var victim = $(this).closest('.victim')
    removeVictim(victim)
  })

  function checkLastVictim() {
    var lastChild = $('#victims-list .victim:not(.template):last-child')
    if (lastChild.length > 0) {
      var name = lastChild.find('input.victim-name').val()
      var contact = lastChild.find('input.victim-contact').val()
      return name || contact
    } else {
      return true
    }
  }

  function getVictims() {
    var victims = []
    $('#victims-list').find('.victim').not('.template').each(function(i) {
      var name = $(this).find('input.victim-name').val()
      var contact = $(this).find('input.victim-contact').val()
      if (name || contact) {
        victims.push({ name: name, contact: contact })
      }
    })
    return victims
  }
})

function addVictimForm() {
    var template = $('#victims-list .victim.template ').clone()
    template.removeClass('template')
    template.find('input').prop('disabled', false)
    template.appendTo('#victims-list')
}

function addAuthorVictim(name, email) {
    $('#author-victim-fields input').prop('disabled', false)
    $('#author-victim-fields #author-victim-name').val(name)
    $('#author-victim-fields #author-victim-contact').val(email)
}

function removeVictim(victim) {
    var name = victim.find('input.victim-name').val()
    var email = victim.find('input.victim-contact').val()
    var modal = $('div.remove-confirmation-modal.victim-modal')

    modal.find('span.remove-confirmation-name').text(name)
    modal.find('span.remove-confirmation-email').text(email)

    var shouldRemove = true

    $('.victim-modal #yes-remove-btn').on('click', function() {
      if (shouldRemove) {
        victim.closest('.victim').remove()
        $("#noosfero-modal").fadeOut(500);
      }
      return false
    })

    $('.victim-modal #no-remove-btn,' +
      '#cboxOverlay, #cboxClose').on('click', function() {
      shouldRemove = false
      $("#noosfero-modal").fadeOut(500);
      return false
    })
}
