var Confirmation = {};

(function() {
  $(document).ready(function() {
    $('#confirmation-step').on('click', '.edit-section', function() {
      StepsBar.jumpTo($(this).data('step'), StepsBar.currentStep())
      return false
    })
  })

  Confirmation = {
    setIdentification: function(name, email, address, anonymous) {
      $('#identification-confirmation .field-value.name').text(name)
      $('#identification-confirmation .field-value.email').text(email)
      $('#identification-confirmation .field-value.address').text(address)
      if (anonymous) {
        $('#identification-confirmation p#anonymous-msg').removeClass('hidden')
        $('#identification-confirmation p#identified-msg').addClass('hidden')
      } else {
        $('#identification-confirmation p#identified-msg').removeClass('hidden')
        $('#identification-confirmation p#anonymous-msg').addClass('hidden')
      }
    },

    setSocioEconomicData: function(birthDate, ethnicity, disability, genderIdentity, genderCondition, sexualOrientation) {
      if(birthDate) {
        $('#identification-confirmation .field-value.birth-date').text(birthDate)
      } else {
        $('#identification-confirmation .section-line.birth-date').hide()
      }

      if(ethnicity) {
        $('#identification-confirmation .field-value.ethnicity').text(ethnicity)
      } else {
        $('#identification-confirmation .section-line.ethnicity').hide()
      }

      if(disability) {
        $('#identification-confirmation .field-value.disability').text(disability)
      } else {
        $('#identification-confirmation .section-line.disability').hide()
      }

      if(genderIdentity) {
        $('#identification-confirmation .field-value.gender-identity').text(genderIdentity)
      } else {
        $('#identification-confirmation .section-line.gender-identity').hide()
      }

      if(genderCondition) {
        $('#identification-confirmation .field-value.gender-condition').text(genderCondition)
      } else {
        $('#identification-confirmation .section-line.gender-condition').hide()
      }

      if(sexualOrientation) {
        $('#identification-confirmation .field-value.sexual-orientation').text(sexualOrientation)
      } else {
        $('#identification-confirmation .section-line.sexual-orientation').hide()
      }
    },

    setOccurrence: function(date, address, details) {
      $('#occurrence-confirmation .field-value.date').text(date)
      $('#occurrence-confirmation .field-value.address').text(address)
      $('#occurrence-confirmation .field-value.details').text(details)
    },

    setOffenders: function(offenders) {
      $('#confirmation-step .offenders-list .entry').not('.template').remove()
      offenders.forEach(function(offender, index) {
        addOffender(offender, index + 1)
      })
    },

    setVictims: function(victims) {
      $('#confirmation-step .victims-list .entry').not('.template').remove()
      victims.forEach(function(victim, index) {
        addVictim(victim, index + 1)
      })
    },

    setMediaFiles: function(files, type) {

      $('#confirmation-step .' + type +'-list .entry').not('.template').remove()

      if(files.size > 0) {
        var index = 1
        files.forEach(function(file) {
          addMediaFile(file.name, type, index++)
        })
        $('#confirmation-step .media-section-' + type).show()
      } else {
        $('#confirmation-step .media-section-' + type).hide()
      }
    },

    displayMediaMessage: function() {
      var files = $('#media-files-confirmation .entry').not('.template')
      if(files.length > 0) {
        $('#media-files-confirmation .no-files-message').hide()
      } else {
        $('#media-files-confirmation .no-files-message').show()
      }
    }
  }

  function addOffender(offender, index) {
    var entry = $('#confirmation-step .offenders-list .entry.template').clone()
    entry.removeClass('template')
    entry.find('.index').text(index)
    entry.find('.offender-name').text(offender.name)
    entry.find('.offender-relationship').text(offender.relationship)
    $('#confirmation-step .offenders-list').append(entry)
  }

  function addVictim(victim, index) {
    var entry = $('#confirmation-step .victims-list .entry.template').clone()
    entry.removeClass('template')
    entry.find('.index').text(index)
    entry.find('.victim-name').text(victim.name)
    entry.find('.victim-contact').text(victim.contact)
    $('#confirmation-step .victims-list').append(entry)
  }

  function addMediaFile(file, type, index) {

    var entry = $('#confirmation-step .' + type + '-list .entry.template').clone()
    entry.removeClass('template')
    entry.find('.index').text(index)
    entry.find('.media-name').text(file)
    $('#confirmation-step .' + type + '-list').append(entry)

  }

})()
