(function() {

  $(document).ready(function() {

    $('#media_files-step .continue-btn').on('click', function() {
      StepsBar.nextStep()
      setConfirmationFiles()
    })

    //Add selected files in form
    $('#media_files-step #media-files').on('change', function() {
      var preview = document.querySelector('#media_files-step ' +
                    '#uploaded-files-image img');
      var files = $('#media_files-step #media-files')[0].files
      var not_added_files = []


      for(var i = 0; i < files.length; i++) {
        if(appendFile(files[i]) != '') {
          not_added_files.push(files[i].name)
        }
      }

      $('#media_files-step #media-files').val("")
      displayMediaSections()
      showFilesErrors(not_added_files)
    })

    //Zoom in images
    $('#media_files-step').on('click', '.zoom-media', function() {
      thumbnail = $(this).find('.thumbnail')
      show(thumbnail)
      noosfero.modal.inline('#show-image')
      return false
    })

    //Remove file
    $('#media_files-step').on('click', '.thumbnail-remove', function() {
      var file = $(this).closest('.denouncement-item')
      file.hide('slow', function() {
        var key = file.data('key')
        var type = file.data('type')
        UploadedFiles.remove(key, type)
        $.when(file.remove()).done(displayMediaSections())
      })
      return false;
    })
  })

  function appendFile(file) {

    if(UploadedFiles.add(file)) {
      var type = UploadedFiles.getType(file)
      var section = $('#media_files-step #uploaded-files-' + type)
      var template = section.find('.denouncement-item.template').clone()

      template.removeClass('template')
      template.data('key', file.name)
      template.find('span.file-name').text(file.name)

      if(UploadedFiles.getType(file) == 'image') {
        previewImage(file, template)
      }

      section.append(template)
      return ''
    } else {
      return file.name
    }
  }

  function previewImage(file, template) {
    var preview = template.find('.thumbnail')
    var reader  = new FileReader()

    reader.onloadend = function () {
      preview.css('background-image', 'url(' + reader.result + ')')
    }

    if (file) {
      reader.readAsDataURL(file)
      preview.title = file.name
    } else {
      preview.src = ""
    }
  }

  function show(thumbnail) {

    var modal = $('#show-image')
    var image = modal.find('.main-image')

    url = thumbnail.css('background-image').replace('url("', '').replace('")', '')
    image.attr('src', url)
  }

  function displayMediaSections() {

    $.each(UploadedFiles.files, function(key, value) {
      var section = $('#media_files-step .' + value.type + '-list')
      if(value.map.size > 0) {
        section.show()
      } else {
        section.hide('slow')
      }
    })
  }

  function showFilesErrors(files) {

    if(files.length > 0) {

      $('#media_files-step .errors-list #duplicate-key-error').remove()
      var error = $('#media_files-step .errors-list #file-duplicate-key').clone()

      error.removeAttr('id')
      error.attr('id', 'duplicate-key-error')

      var list = error.find('ul')

      $.each(files, function(index) {
        var li = error.find('li.error-file.template').clone()
        li.removeClass('template')
        li.text(files[index])
        list.append(li)
      })

      $('#media_files-step .errors-list').append(error)
      showErrorMessage('#media_files-step .error-msg', '#media_files-step ' +
                       '.errors-list #duplicate-key-error')
    }
  }

  function setConfirmationFiles() {

    $.each(UploadedFiles.files, function(key, value) {
      Confirmation.setMediaFiles(value.map, value.type)
    })
    Confirmation.displayMediaMessage()
  }

})()
