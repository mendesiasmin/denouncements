function Store(name, version) {
  this.name = name
  this.version = version || "1"
  this.storeName = 'default'

  this.setStoreName = function(storeName) {
    this.storeName = storeName
    return this
  }

  this.connect = function() {
    var self = this
    return new Promise(function(resolve, reject) {
      var request = window.indexedDB.open(self.name, self.version)
      request.onerror = function(event) {
        var error = event.taget.error
        reject("[IndexedDB ERROR] not posible to connect: " + error)
      }

      request.onsuccess = function(event) {
        resolve(event.target.result)
      }

      request.onupgradeneeded = function(event) {
        var conn = event.target.result;
        // Creates the object store if it doesn't exist
        if (!conn.objectStoreNames.contains(self.storeName)) {
          conn.createObjectStore(self.storeName, { autoIncrement: true })
        }
      }
    })
  }

  this.write = function(data, key) {
    var self = this
    return new Promise(function(resolve, reject) {
      return self.connect().then(function(conn) {
        var request = conn.transaction([self.storeName], "readwrite")
                          .objectStore(self.storeName)
                          .put(data, key)
        request.onsuccess = function(evt) { resolve(evt) }
        request.onerror = function(evt) { reject(evt.target.error) }
      }).catch(function(error) {
        reject(error)
      })
    })
  }

  this.listItems = function() {
    var self = this
    return new Promise(function(resolve, reject) {
      return self.connect().then(function(conn) {
        var request = conn.transaction([self.storeName])
                          .objectStore(self.storeName)
        var cursor = request.openCursor()

        var items = {}
        cursor.onsuccess = function(evt) {
          var item = evt.target.result
          if (item) {
            items[item.key] = item.value
            item.continue()
          } else {
            resolve(items)
          }
        }
        cursor.onerror = function(evt) { reject(evt.target.error) }
      }).catch(function(error) {
        reject(error)
      })
    })
  }

  this.remove = function(key) {
    var self = this
    return new Promise(function(resolve, reject) {
      return self.connect().then(function(conn) {
        var request = conn.transaction([self.storeName], "readwrite")
                          .objectStore(self.storeName)
                          .delete(key)
        request.onsuccess = function() { resolve() }
        request.onerror = function(evt) { reject(evt.target.error) }
      }).catch(function(error) {
        reject(error)
      })
    })
  }
}
