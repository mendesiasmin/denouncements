class DenouncementsPlugin::Denouncement < ApplicationRecord

  module Status
    PENDING  = 0 # The denouncement was sent to the external service
    UPDATED  = 1 # The denouncement was updated by the external service

    N_('pending')
    N_('updated')

    def data(status, key, protocol = '')
      base = {
        PENDING => {
          'message' => _("Your denouncement will be updated soon."),
          'title' => _('New denouncement'),
          'value' => 'pending'
        },
        UPDATED => {
          'message' => _("Denouncement protocol: #{protocol}"),
          'title' => _('Your denouncement was updated'),
          'value' => 'updated'
        }
      }
      base[status][key]
    end

    def status_message
      data(self.status, 'message', self.protocol)
    end

    def status_title
      data(self.status, 'title', self.protocol)
    end

    def status_value
      data(self.status, 'value', self.protocol)
    end
  end

  attr_accessible :token, :date, :protocol, :status, :city, :description,
                  :address

  include DenouncementsPlugin::Denouncement::Status

  def notification_data
    { 'protocol' => protocol, 'type' => 'DENOUNCEMENT_UPDATE' }
  end

  belongs_to :denouncer, class_name: 'DenouncementsPlugin::Denouncer'
  belongs_to :user
  has_many :media_files, class_name: 'DenouncementsPlugin::MediaFile'

  store_accessor :metadata
  after_save :send_denouncement_to_external_service
  before_save :add_token
  before_validation :add_status


  validates_presence_of :date, :status, :address, :description
  validate :victims_are_valid
  validate :offenders_are_valid
  validate :denouncer_is_victim

  def send_denouncement_to_external_service
    unless protocol
      data = { 'denouncement' => attributes.keep_if { |attr| attr != 'id' } }
      Delayed::Job.enqueue DenouncementsPlugin::SendDenouncementJob.new(data)
    end
  end

  def tokens
    denouncer.device_tokens
  end

  private

  def add_token
    self.token ||= SecureRandom.base64
  end

  def add_status
    self.status ||= Status::PENDING
  end

  def victims_are_valid
    validate_records(:victims, [:name, :contact])
  end

  def offenders_are_valid
    validate_records(:offenders, [:name, :institution, :relationship])
  end

  def denouncer_is_victim
    # TODO: if denouncer is victim, check if its in metadata[:victims]
  end

  def validate_records(field, fields)
    entries = metadata[field.to_s]
    if entries.blank?
      errors.add field, _('cannot be empty')
      return
    end

    entries.each do |entry|
      if fields.all? { |f| entry[f.to_s].blank? }
        errors.add field, _('one of the entries is not valid')
        break
      end
    end
  end
end
