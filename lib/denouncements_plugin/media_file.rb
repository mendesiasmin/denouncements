class DenouncementsPlugin::MediaFile < ApplicationRecord

  include UploadSanitizer

  attr_accessible :uploaded_data, :denouncement

  belongs_to :denouncement, class_name: 'DenoucementsPlugin::Denouncement'

  def self.max_size
    5.megabytes
  end

  UPLOAD_FOLDER = 'public/articles/plugins/denouncements'

  has_attachment :storage => :file_system,
    :max_size => max_size,
    :path_prefix => UPLOAD_FOLDER

end
