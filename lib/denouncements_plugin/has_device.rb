module DenouncementsPlugin::HasDevice
  extend ActiveSupport::Concern

  included do
    has_many :devices, class_name: 'DenouncementsPlugin::Device', as: :owner

    def self.device_tokens
      joins(:devices).pluck(:tokens).flatten.uniq
    end
  end

  def update_devices(uuid, token)
    # If token is nil, a reference to the device will still be saved
    return unless uuid.present?
    token = token.blank? ? nil : token
    device = devices.find_or_initialize_by(uuid: uuid)
    device.tokens |= [token].compact
    device.save
  end

  def device_tokens
    self.class.where(id: self.id).device_tokens
  end
end
