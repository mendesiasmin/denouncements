class CommunitiesListBlock < Block

  attr_accessible :accessor_id, :accessor_type, :role_id, :resource_id, :resource_type

  def self.description
    _("<p>Display a profile's communities list.</p>")
  end

  def self.short_description
    _('Communities list')
  end

  def self.pretty_name
    _('Communities list')
  end

  def default_title
    _('Communities')
  end

  def help
    _("This block displays a profile's communities list.")
  end

end
