class DenouncementsPlugin::Denouncer < ApplicationRecord

  include DenouncementsPlugin::HasDevice

  attr_accessible  :name, :email, :profile_id
  validates_presence_of :name, :email

  has_many :denouncements, class_name: 'DenouncementsPlugin::Denouncement'
  belongs_to :profile

end
