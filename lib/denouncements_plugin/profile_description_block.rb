class ProfileDescriptionBlock < Block

  attr_accessible :accessor_id, :accessor_type, :role_id, :resource_id, :resource_type

  def self.description
    _("<p>Display your profile description.</p>")
  end

  def self.short_description
    _('Profile Description')
  end

  def self.pretty_name
    _('Profile Description')
  end

  def default_title
    _('Profile')
  end

  def help
    _('This block displays a profile information.')
  end

end
