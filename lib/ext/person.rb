require_dependency 'person'

class Person

  include DenouncementsPlugin::HasDevice

  has_many :denouncements, class_name: 'DenouncementsPlugin::Denouncement'

end
