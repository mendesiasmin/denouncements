require "test_helper"

class PushTokenTest < ActionDispatch::IntegrationTest

  include DenouncementsPlugin::CookiesHelper

  should 'add token to device after denouncement was created' do
    cookies[device_id_cookie_key] = '123fad'
    post '/plugin/denouncements/create', {
      person: { name: 'ze', email: 'ze@mail.com' },
      dncmt: {
        date: DateTime.now,
        city: 'Foo',
        description: 'a violation'
      },
      victims: [{ name: 'maria', contact: 'maria@mail.com' }],
      offenders: [{ name: 'rose', institution: 'foo', relationship: '?' }]
    }

    denouncer = DenouncementsPlugin::Denouncer.last
    assert denouncer.device_tokens.empty?

    post '/plugin/denouncements/push_token/set', token: '123123',
                                                 uuid: '123fad'
    assert_includes denouncer.device_tokens, '123123'
  end

end
