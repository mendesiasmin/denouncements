require 'test_helper'

class ArticleTest < ActiveSupport::TestCase

  def setup
    @profile = create_user.person
    @folder = fast_create(Folder)
  end

  should 'send notification if the article is published' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.expects(:notify_publication).once
    article.save
  end

  should 'send notification if the article is updated to be published' do
    article = Article.new(name: 'article', profile: @profile, published: false)
    article.expects(:notify_publication).never
    article.save

    article.published = true
    article.expects(:notify_publication).once
    article.save
  end

  should 'not send notification if the article is not being published' do
    article = Article.new(name: 'article', profile: @profile, published: false)
    article.expects(:notify_publication).never
    article.save
  end

  should 'not send notification if the article is being edited' do
    article = Article.new(name: 'hi', profile: @profile, published: true)
    article.expects(:notify_publication).once
    article.save

    article.body = 'updates should not trigger a notification'
    article.expects(:notify_publication).never
    article.save
  end

  should 'deliver notification if article is supposed to' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.expects(:send_notifications?).returns(true)
    article.expects(:notify).once
    article.save
  end

  should 'not deliver notification if it is a folder' do
    folder = Folder.new(name: 'folder', profile: @profile, published: true)
    folder.stubs(:send_notifications?).returns(true)
    folder.expects(:notify).never
    folder.save
  end

  should 'not deliver notification if article is not supposed to' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.expects(:send_notifications?).returns(false)
    article.expects(:notify).never
    article.save
  end

  should 'send notifications if it is a Folder and the option is active' do
    refute @folder.send_notifications?
    @folder.denouncements_metadata['should_notify'] = '1'
    assert @folder.send_notifications?
  end

  should 'not send notifications if parent is not a folder' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.stubs(:parent).returns(nil)
    refute article.send_notifications?
  end

  should 'not send notifications if parent is a folder but the option is '\
         'not active' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.stubs(:parent).returns(@folder)
    @folder.denouncements_metadata['should_notify'] = '1'
    article.denouncements_metadata['should_notify'] = '0'
    refute article.send_notifications?
  end

  should 'send notifications if parent is a folder and the option is active' do
    article = Article.new(name: 'article', profile: @profile, published: true)
    article.stubs(:parent).returns(@folder)
    article.denouncements_metadata['should_notify'] = '1'
    assert article.send_notifications?
  end

  should 'not use parent configuration if article was already saved '\
         'but option is still nil' do
    article = @profile.articles.create(name: 'article', published: true)
    article.stubs(:parent).returns(@folder)
    @folder.denouncements_metadata['should_notify'] = '1'
    article.denouncements_metadata['should_notify'] = nil
    refute article.send_notifications?
  end

  should 'notify all profile members when new public content is created' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    person2 = fast_create(Person, name: 'bla')

    DenouncementsPlugin::Device.create!(uuid: '123fad', tokens: ['123'],
                                        owner: person1)
    DenouncementsPlugin::Device.create!(uuid: '123fax', tokens: ['456'],
                                        owner: person2)
    community = fast_create(Community, name: 'my_community', identifier: 'my_community')
    article = fast_create(Article, name: 'article',
                          published: true,
                          content_access: AccessLevels.levels[:users],
                          profile_id: community.id)

    DenouncementsPlugin.expects("config").at_most(2).returns({'communities' => 'my_community'})

    assert Article.last.tokens.include? person1.device_tokens.first
    assert Article.last.tokens.include? person2.device_tokens.first
  end

  should 'not notify profile members when new private content is created' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    person2 = fast_create(Person, name: 'bla')

    DenouncementsPlugin::Device.create!(uuid: '123fad', tokens: ['123'],
                                        owner: person1)
    DenouncementsPlugin::Device.create!(uuid: '123fax', tokens: ['456'],
                                        owner: person2)
    community = fast_create(Community, name: 'my_community', identifier: 'my_community')
    article = fast_create(Article, name: 'article',
                          published: true,
                          content_access: AccessLevels.levels[:self],
                          profile_id: community.id)

    DenouncementsPlugin.expects("config").at_most(2).returns({'communities' => 'my_community'})

    refute Article.last.tokens.include? person1.device_tokens.first
    refute Article.last.tokens.include? person2.device_tokens.first
  end

  should 'notify only members in profile exceptions list' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    person2 = fast_create(Person, name: 'bla')

    DenouncementsPlugin::Device.create!(uuid: '123fad', tokens: ['123'],
                                        owner: person1)
    DenouncementsPlugin::Device.create!(uuid: '123fax', tokens: ['456'],
                                        owner: person2)
    community = fast_create(Community, name: 'my_community',
                            identifier: 'my_community')
    article = fast_create(Article, name: 'article',
                          published: true,
                          content_access: AccessLevels.levels[:self],
                          profile_id: community.id)
    article.article_privacy_exceptions << person2
    article.save!
    DenouncementsPlugin.expects("config").at_most(2).returns({'communities' => 'my_community'})

    refute Article.last.tokens.include? person1.device_tokens.first
    assert Article.last.tokens.include? person2.device_tokens.first
  end

  should 'send properly notification when a text article is created' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    person2 = fast_create(Person, name: 'bla')

    community = fast_create(Community, name: 'my_community',
                            identifier: 'my_community')

    article = fast_create(TextArticle, name: 'new article',
                          published: true,
                          content_access: AccessLevels.levels[:users],
                          profile_id: community.id,
                          author_id: person1.id)

    article.save!
    message = article.push_notification_message
    assert_equal message, "A new text article was published by #{article.author.name}."
  end

  should 'send properly notification when a folder is created' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    person2 = fast_create(Person, name: 'bla')

    community = fast_create(Community, name: 'my_community',
                            identifier: 'my_community')

    folder = fast_create(Folder, name: 'new folder',
                          published: true,
                          content_access: AccessLevels.levels[:users],
                          profile_id: community.id,
                          author_id: person1.id)

    message = folder.push_notification_message
    assert_equal message, "A new folder was created by #{folder.author.name}."
  end

  should 'catch error if created content is not valid' do
    Environment.default.enable_plugin(DenouncementsPlugin)

    person1 = fast_create(Person, name: 'foo')
    community = fast_create(Community, name: 'my_community',
                            identifier: 'my_community')

    event = create(Event, :name => 'e1', :start_date => DateTime.new(2008,1,15),
                   :profile => community, :author => person1)

    message = event.push_notification_message
    assert_equal message, "No notification message available."
  end
end
