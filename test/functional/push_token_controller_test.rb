require 'test_helper'

class PushTokenControllerTest < ActionController::TestCase

  include DenouncementsPlugin::CookiesHelper

  should 'return bad request status if param is not correct' do
    post :set, not_token: 'mytoken'
    assert_response :bad_request
  end

  should 'set token in the session' do
    post :set, token: 'mytoken', uuid: '123456'
    assert_response :ok
    assert_equal 'mytoken', cookies[push_token_cookie_key]
  end

  should 'set id in the session' do
    post :set, token: 'mytoken', uuid: '123456'
    assert_response :ok
    assert_equal '123456', cookies[device_id_cookie_key]
  end

  should 'set token for the user if the user is logged in' do
    person = create_user.person
    login_as(person.identifier)

    post :set, token: 'mytoken', uuid: '123456'
    person.reload
    assert_includes person.device_tokens, cookies[push_token_cookie_key]
  end

  should 'add token to devices with existent uuid' do
    device1 = DenouncementsPlugin::Device.create(uuid: '123456',
                                                 tokens: ['oldtoken1'],
                                                 owner: fast_create(Person))
    device2 = DenouncementsPlugin::Device.create(uuid: '123456',
                                                 tokens: ['oldtoken2'],
                                                 owner: fast_create(Person))

    post :set, token: 'mytoken', uuid: '123456'
    device1.reload
    assert_equivalent ['mytoken', 'oldtoken1'], device1.tokens
    device2.reload
    assert_equivalent ['mytoken', 'oldtoken2'], device2.tokens
  end

end
