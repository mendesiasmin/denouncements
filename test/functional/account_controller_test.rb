require 'test_helper'

class AccountControllerTest < ActionController::TestCase

  include DenouncementsPlugin::CookiesHelper

  def setup
    Environment.default.enable_plugin(DenouncementsPlugin)
  end

  should 'add push token to profile after login' do
    cookies[push_token_cookie_key] = 'mytoken'
    cookies[device_id_cookie_key] = '123456'
    user = create_user
    user.activate

    post :login, user: { login: user.login, password: user.login }
    user.reload
    assert_includes user.person.device_tokens, 'mytoken'
  end

end
